import sublime, sublime_plugin, re

class ChecklistBase(sublime_plugin.TextCommand):
    def run(self, edit):
        self.settings = sublime.load_settings("Checklist.sublime-settings")
        self.incomplete_mark = self.settings.get("checklist.incomplete_mark")
        self.complete_mark = self.settings.get("checklist.complete_mark")
        self.incomplete_regex = self.settings.get("checklist.incomplete_regex")
        self.complete_regex = self.settings.get("checklist.complete_regex")
        self.non_whitespace_regex = self.settings.get("checklist.non_whitespace_regex")
        self.either_regex = self.settings.get("checklist.either_regex")
        self.mark_length = 4
        self.runCommand(edit)

    def replace(self, edit, region, match, replacement):
        line = self.view.line(region)
        start = match.start(0)
        print("Length of replacement = " + str(len(match.group(0))))
        target = sublime.Region(line.a + start, line.a + start + len(match.group(0)))
        self.view.replace(edit, target, replacement)

    def insert(self, edit, region, match, replacement):
        line = self.view.line(region)
        start = match.start(0)
        target = sublime.Region(line.a + start, line.a + start)
        self.view.insert(edit, target.a, replacement)

    
class ChecklistSwitchCommand(ChecklistBase):
    def runCommand(self, edit):
        for region in self.view.sel():
            if region.empty():
                line = self.view.line(region)
                string = self.view.substr(line)
                unchecked = re.search(self.incomplete_regex, string)
                checked = re.search(self.complete_regex, string)
                if(unchecked):
                    self.replace(edit, region, unchecked, self.complete_mark)
                    continue
                elif(checked):
                    self.replace(edit, region, checked, self.incomplete_mark)
                    continue

class ChecklistCreateCommand(ChecklistBase):
    def runCommand(self, edit):
        for region in self.view.sel():
            line = self.view.line(region)
            string = self.view.substr(line)
            either = re.search(self.either_regex, string)
            if(either):
                self.replace(edit, region, either, '')
                continue
            else:
                none = re.search(self.non_whitespace_regex, string)
                self.insert(edit, region, none, self.incomplete_mark)
                continue