# Checklist

A simple task/todo list plugin for Sublime Text

## Usage

Simply save a file with the extension **.checklist** and use the 2 simple shortcuts to create and manage checklist items. Items use the Github-flavoured markdown format of square bracket pairs to indicate items. Those with a blank space are incomplete, those with an 'x' are complete.

As a small convenience, the file format also supports any string followed by a colon as a header.

## Shortcuts

The default shortcuts are: -

- ctrl+alt+c creates or removes a checkbox from the beginning of a line of text.
- ctrl+alt+x toggles a checklist item between complete and incomplete

That's it!!  Easy as pie. I wanted something simple and functional with as little bloat as possible. I also wanted to leqarn about Python and Sublime plugin development.